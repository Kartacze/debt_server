const Transfer = require('../models/transfer');
const User = require('../models/user');

// 1. the user is the sender or receiver

exports.sendMoney = function(req, res, next) {
  const user = req.user.email;
  const { to, from, value } = req.body;

  if (!to || !from || !value) {
    return res.status(422).send({error: 'Your request form is invalid, pls provide propers e-mails and value'});
  } else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(to)
    || !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(from)) {
    return res.status(422).send({error: 'Invalid emails, pls check'});
  } else if (user != to && user != from) {
    return res.status(422).send({error: 'You can request only your transactions'});
  } else if (isNaN(value) || value <= 0) { // isNaN is not enought
    return res.status(422).send({error: 'I think that the value is not correct'});
  } else {
    User.find({
      'email': {
        $in: [to, from]
      }
    }, function(err, docs) {
      if(!docs[1]) {
        return res.send({ error: `All users have not been found in database` });
      } else {

        const money = new Transfer({
            to,
            from,
            value,
            accepted: { who: user }
        });

        money.save(function(err) {
            if (err) { return next(err) }
            return res.status(422).send({resp: 'ALl right !!!'});
        });

        docs[0].requests.push(money._id);
        docs[1].requests.push(money._id);
        docs[0].save((err) => {if (err) console.log('writing error: ', err)});
        docs[1].save((err) => {if (err) console.log('writing error: ', err)});

      }
    });
  }
};

exports.acceptMoney = (req, res, next) => {
  const { id } = req.params;
  Transfer.findById(id, (err, doc) => {
    if (doc) { // here I assume that both users are available. However the user might have been deleted
      User.find({
        'email': {
          $in: [ doc.from, doc.to ]
        }
      }, (err, docs) => {
        if(doc.from === req.user.email || doc.to === req.user.email) {
          if (doc.accepted[0].who != req.user.email) {
            docs[0].requests = docs[0].requests.filter((e) => e != id);
            docs[1].requests = docs[1].requests.filter((e) => e != id);
            docs[0].money = docs[0].money - doc.value;
            docs[1].money = docs[1].money + doc.value;
            docs[0].save();
            docs[1].save();
            doc.remove();
            return res.json({ rest: 'Rather ok'});
          } else {
            return res.json({ error: 'this user have already accepted'});
          }
        } else {
          return res.json({ error: 'wrong user'});
        }
      });
    } else {
      return res.json({ error: 'This transfer id have not been found'});
    }
  });
};

exports.rejectMoney = (req, res, next) => {

};
