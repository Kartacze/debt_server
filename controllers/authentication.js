const jwt = require('jwt-simple');
const User = require('../models/user');
const config = require('../config');

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
}

exports.signin = function(req, res, next) {
    // user have email and password auth
    // we need to give them a token
    res.send({ token: tokenForUser(req.user) });
}

exports.signup = function(req, res, next) {

    console.log('some error here', req.body);
    const { email, password } = req.body;

    if (!email || !password) {
        return res.status(400).send({ error: 'You must provide email and password' });
    }

    // see if user with given email exists
    User.findOne({ email }, function(err, existingUser) {
        if(err) {
          console.log('some error here');
          return next(err); }

        // if a user with emai does exists, return an error
        if(existingUser) {
          return res.status(422).send({error: 'Email is in use'});
        }

        console.log({ email, password });

        User.create({ email, password }, function(err, user) {
          if (err) {
            console.log('some error while saving', err);
            return next(err);
          }
          // Respond to reqest indicating the user was created
          res.json({token: tokenForUser(user)});
        });

        // user.save(function(err) {
        //     if (err) {
        //       console.log('some error while saving');
        //       return next(err) }
        //     // Respond to reqest indicating the user was created
        //     res.json({token: tokenForUser(user)});
        // });
    });
};
