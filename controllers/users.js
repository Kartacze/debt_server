const User = require('../models/user');

exports.getMe = (req, res, next) => {
  let { email } = req.user;
  User.findOne({ email }, function(err, doc) {
    if (err) { return res.status(422).send({ error: 'could not find user with this email' }); }
    delete doc.password;
    let { email, friends, requests, _id } = doc;
    return res.json({ email, friends, requests, id: _id });
  });
}

// exports.getUser = (req, res, next) => {
//   const { email, friends } = req.user;
//   return res.json({ email, friends });
// }
//
// exports.getUsers = (req, res, next) => {
//   console.log('getUser', req.user.email);
//   const { email, friends } = req.user;
//   return res.json({ email, friends });
// }

// exports.getFriends = function(req, res, next) {
//   console.log('getFriends', req.user);
//   const email = req.user.email;
//
//   if (!email) {
//     return res.status(422).send({error: 'You must provide user email'});
//   }
//
//   // see if user with given email exists
//   User.findOne({
//     email
//   }, function(err, existingUser) {
//     if (err) {
//       return next(err);
//     }
//     // if a user with emai does exists, return an error
//     if (existingUser) {
//       console.log(existingUser);
//       return res.send({users: existingUser.friends});
//     } else {
//       console.log('no user found');
//       return res.status(422).send({error: 'No user with this email'});
//     }
//   });
// };

// There should be checked:
// 1. No duplicate 2. check if users exists 3.
// And fixed:
// 1. handling many async calls synchronously

exports.addFriend = function(req, res, next) {
  const email = req.user.email;
  const { buddy } = req.body;

  console.log('email, buddy', email, buddy);

  if (!email || !buddy) {
    return res.status(422).send({error: 'You must provide email and new friend'});
  }

  User.find({
    'email': {
      $in: [email, buddy]
    }
  }, function(err, docs) {
    if(!docs[0]) {
      return res.send({ error: `cannot find user: ${email}` });
    } else if (!docs[1]) {
      return res.send({ error: `cannot find user: ${buddy}` });
    } else {

      const request = new friendReq({
          email,
          new: buddy,
      });

      request.save(function(err) {
          if (err) { return next(err) }
          return res.json({ resp: 'everythin gonna be ok'});
      });
      console.log(request._id);
      docs[0].friends_reqs.push(request._id);
      docs[1].friends_reqs.push(request._id);
      docs[0].save((err) => {if (err) console.log('writing error: ', err)});
      docs[1].save((err) => {if (err) console.log('writing error: ', err)});
    }
  });
};

const friendsOperation = (id, req, res, type) => {
  friendReq.findById(id, (err, doc) => {
    if (doc) {
      User.find({
        'email': {
          $in: [ doc.email, doc.new]
        }
      }, (err, docs) => {
        console.log('user', req.user);
        console.log(doc.email);
        if(doc.new === req.user.email) {
          docs[0].friends_reqs = docs[0].friends_reqs.filter((e) => e != id);
          docs[1].friends_reqs = docs[1].friends_reqs.filter((e) => e != id);
          if (type === 'add') {
            docs[0].friends.push(doc.new);
            docs[1].friends.push(doc.email);
          }
          docs[0].save();
          docs[1].save();
          doc.remove();
          return res.json({ rest: 'Rather ok'});
        } else {
          return res.json({ error: 'wrong user - this is implementations error'});
        }
      });
    } else {
      return res.json({ error: 'This id have not been found'});
    }
  });
}

// problem: user may change someone elses request
exports.acceptFriend = (req, res, next) => {
  const { id } = req.params;
  const { status } =req.body;
  if (status === 'accept') {
    friendsOperation(id, req, res, 'add');
  } else {
    friendsOperation(id, req, res, '');
  }
}

// exports.getFriendsRequests = (req, res, next) => {
//   const email = req.user.email;
//   User.findOne({ email }, 'friends_reqs', function (err, person) {
//     if (err) return handleError(err);
//     if (person) {
//       friendReq.find({
//         '_id': {
//           $in: person.friends_reqs
//         }
//       }, 'new email date', (err, docs) => {
//         return res.json({ resp: docs });
//       });
//     } else {
//       return res.json({ resp: [] });
//     }
//   })
// }
