const passport = require('passport');
const User = require('../models/user');
const config = require('../config');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

// local strategy with email and password
const LocalStrategy = require('passport-local');

const localOptions = { usernameField: 'email' };

const localLogin = new LocalStrategy(localOptions, function(email, password, done) {
    // Verify this email and password, call done with the user
    // if it is the corret email and pasword
    // otherwise, call done with false
    console.log('email', email, 'passport', password);
    User.findOne( { email }, function(err, user) {
      console.log('user in localLogin', user);
        if (err)  { return done(err); }
        if(!user) { console.log('no such user');
        return done(null, false); }

        user.comparePassword(password, function(err, isMatch) {
            if (err) { return done(err); }
            if(!isMatch) {
              console.log('pass not match');
            return done(null, false)}

            return done(null, user);
        })
    });

});

// Options for JWT Strategy
const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: config.secret,
};

// Create JWT Strategy
const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done) {
    // See if the user ID in the payload exists in our database
    // If it does, call 'done' with that other
    // otherwise, call done without a user object
    User.findById(payload.sub, function(err, user) {
        if (err) { return done(err, false); }
        if (user) {
            done(null, user);
        } else {
            done(null, false);
        }
    });
});


// Tell passport to user this strategy
passport.use(jwtLogin);
passport.use(localLogin);
