const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define model
// totaly mad :O
const transferSchema = new Schema({
  from: { type: String, lowercase: true },
  to: { type: String, lowercase: true },
  date: { type: Date, default: Date.now, required: true },
  value: { type: Number },
  accepted: [{ who: { type: String, lowercase: true }, date: { type: Date, default: Date.now } }]
});

transferSchema.pre('save', function(next) {
  const trans = this;
  trans.date = new Date();
  trans.accepted.date = new Date();
  next();
});

const ModelClass = mongoose.model('transfers', transferSchema);
module.exports = ModelClass;
