const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

// Define model
const userSchema = new Schema({
    email: { type: String, unique: true, lowercase: true },
    password: String,
    // for every friend there is wallet for him
    friends: [ { email: String, wallet: Schema.Types.ObjectId } ],
    requests: [ { email: { type: String, unique: false, lowercase: true },
      date: { type: Date, default: Date.now, required: false }
    } ],
});

// On Save Hook, encrypt passwd
userSchema.pre('save', function(next) {
  const user = this;
  if (this.isModified('password') || this.isNew) {

    bcrypt.genSalt(10, function(err, salt) {
        if (err) {
          console.log('gen salt error');
          return next(err); }

        bcrypt.hash(user.password, salt, null, function(err, hash) {
            if (err) {
              console.log('bcrypt.hash error');
              return next(err); }
            user.password = hash;
            next();
        });
    });
  } else {
    console.log('this is not new user');
    next();
  }
  console.log('pre save ok');
});

userSchema.methods.comparePassword = function(candidatePassword, callback) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) { return callback(err); }
        callback(null, isMatch);
    })
}

// Create model class
const ModelClass = mongoose.model('user', userSchema);

// export the model
module.exports = ModelClass;
