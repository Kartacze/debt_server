const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define model
// totaly mad :O
const requestSchema = new Schema({
  from: { type: String, lowercase: true },
  to: { type: String, lowercase: true },
  date: { type: Date, default: Date.now, required: true },
  value: { type: Number },
  accepted: [{ who: { type: String, lowercase: true }, date: { type: Date, default: Date.now } }]
});

const ModelClass = mongoose.model('request', requestSchema);
module.exports = ModelClass;
