const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define model
const walletSchema = new Schema({
  user1: String,
  user2: String,
  state: Number, // for user1, user have -state
  history: [ Number ], // to be continued...
});

const ModelClass = mongoose.model('wallet', walletSchema);
module.exports = ModelClass;
