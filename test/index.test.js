const mongoose = require("mongoose");
const User = require('../models/user');
const Authentication = require('../controllers/authentication');
const Schema = mongoose.Schema;

//Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const should = chai.should();
chai.use(chaiHttp);

let token1 = '';
let token2 = '';

//Our parent block
describe('Authentication', () => {
  const testUser1 = { email : "test1@gma.com", password : "dupa" };
  const testUser2 = { email : "asdcdsds@wer.com", password : "asdasczd" };
  const testUser3 = { email : "test3@gma.com", password : "dupa" };
  const testUserFake = { email : "fake@user.com", password : "none" };
  const testUser1OnlyEmail = { email : "teddek2@gma.com" };
  const testUser1WrongPassword = { email : "teddek@gma.com", password : "dupa23" };

  after(function() {
    User.remove({ $or: [ { email: testUser1.email },
        { email: testUser2.email }, { email: testUser3.email } ] }, function(err) {
      if (err) { console.log('error /signup user', err) }
    })
  });

  describe('/signup user', () => {
    it('it should add the user', (done) => {
      chai.request(server)
        .post('/signup')
        .type('application/json')
        .send(testUser1)
        .end(function(err, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('token');
          res.body.token.should.be.a('string');
          token1 = res.body.token;
          done();
        });
    });
  });

  describe('/signup the same user second time', () => {
    it('it should not add the user', (done) => {
      chai.request(server)
        .post('/signup')
        .type('application/json')
        .send(testUser1)
        .end(function(err, res) {
          res.should.have.status(422);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          done();
        });
    });
  });

  describe('/signup user with only email', () => {
    it('it should not add the user', (done) => {
      chai.request(server)
        .post('/signup')
        .type('application/json')
        .send(testUser1OnlyEmail)
        .end(function(err, res) {
          res.should.have.status(400);
          res.body.should.be.a('object');
          res.body.error.should.be.a('string');
          done();
        });
    });
  });

  describe('/signin user', () => {
    it('it should return token', (done) => {
      chai.request(server)
        .post('/signin')
        .type('application/json')
        .send(testUser1)
        .end(function(err, res) {
          console.log('this is my response: ', res.body);
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('token');
          done();
        });
    });
  });

  describe('/signin user', () => {
    it('it should not return token', (done) => {
      chai.request(server)
        .post('/signin')
        .type('application/json')
        .send(testUser1WrongPassword)
        .end(function(err, res) {
          console.log('this is my response: ', res.body);
          res.should.have.status(401);
          done();
        });
    });
  });

  describe('/signup user', () => {
    it('it should add second user', (done) => {
      chai.request(server)
        .post('/signup')
        .type('application/json')
        .send(testUser2)
        .end(function(err, res) {
          res.should.have.status(200);
          token2 = res.body.token;
          done();
        });
    });
  });

  describe('/signup user', () => {
    it('it should add third user', (done) => {
      chai.request(server)
        .post('/signup')
        .type('application/json')
        .send(testUser3)
        .end(function(err, res) {
          res.should.have.status(200);
          done();
        });
    });
  });

  describe('/me get the user', () => {
    it('it should return the user data', (done) => {
      chai.request(server)
        .get('/me')
        .type('application/json')
        .set('Authorization', token1)
        .end(function(err, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.a.property('email');
          res.body.email.should.be.a('string');
          res.body.email.should.equal(testUser1.email);
          res.body.should.have.a.property('requests');
          res.body.requests.should.be.a('array');
          res.body.should.have.a.property('friends');
          res.body.friends.should.be.a('array');
          res.body.should.not.have.a.property('password');
          res.body.should.have.a.property('id');
          res.body.id.should.be.a('string');
          done();
        });
    });
  });

  describe('/me get the user', () => {
    it('it should return unauthorized', (done) => {
      chai.request(server)
        .get('/me')
        .type('application/json')
        .end(function(err, res) {
          res.should.have.status(401);
          done();
        });
    });
  });

  // 1. Add new friend request

  // 2. refuse request

  // 3. add new request

  // 4. accept request

});
