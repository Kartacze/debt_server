const Authentication = require('./controllers/authentication');
const Users = require('./controllers/users');
// const Transfers = require('./controllers/transfers');


const passportService = require('./services/passport');
const passport = require('passport');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

module.exports = function (app) {
  app.post('/signup', Authentication.signup);
  app.post('/signin', requireSignin, Authentication.signin);

  // me returns whole user object
  app.get('/me', requireAuth,  Users.getMe);

  // probably there will be no access to other users, maybe only the users list
  // app.get('/users/:email', requireAuth,  Users.getUsers);

  // friends part
  // app.get('/friends', requireAuth,  Users.getFriends);
  // app.get('/friends/requests', requireAuth,  Users.getFriendsRequests);
  // app.post('/friends/add', requireAuth, Users.addFriend); // this have to be changed to /frineds
  // app.post('/friends/requests/:id', requireAuth, Users.acceptFriend);

  // app.get('/requests', requireAuth,  Users.getRequests);
  // app.get('/friends/requests', requireAuth,  Users.getFriendsRequests);
  // app.post('/friends/add', requireAuth, Users.addFriend); // this have to be changed to /frineds
  // app.post('/friends/requests/:id', requireAuth, Users.acceptFriend);

  // money transfer part
  // app.post('/transfer/send', requireAuth, Transfers.sendMoney);
  // app.post('/transfer/accept/:id', requireAuth, Transfers.acceptMoney);
  // app.post('/transfer/reject/:id', requireAuth, Transfers.rejectMoney);

  app.get('/', requireAuth, function (req, res, next) {
      res.send({ hi: 'there' })
  });
}
